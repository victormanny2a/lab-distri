package ejercicio82;

public class Date {
	Long cedula;
	String nombre;
	String apellido;
	String chapa;
	String marca;
	
	public Date() {

	}

	public Date(Long cedula, String nombre, String apellido, String chapa, String marca) {
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.chapa = chapa;
		this.marca = marca;
	}

	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getChapa() {
		return chapa;
	}

	public void setChapa(String chapa) {
		this.chapa = chapa;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
}
