package ejercicio82;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class DateJSON {
	
	
    public static void main(String[] args) throws Exception {
    	DateJSON representacion = new DateJSON();
    	
    	System.out.println("Ejemplo de uso 1: pasar de objeto a string");
    	Date d = new Date();
    	d.setNombre("Maria");
    	d.setApellido("Gomez");
    	d.setCedula(123456L);
    	d.setChapa("123ABC");
    	d.setMarca("Toyota");
    	
    	String r1 = representacion.objetoString(d);
    	System.out.println(r1);
    	
    	
    	System.out.println("\n*************************************************************************");
    	System.out.println("\nEjemplo de uso 2: pasar de string a objeto");
    	String un_string = "{\"cedula\":123123,\"nombre\":\"Ana\",\"apellido\":\"Perez\",\"chapa\":\"DEF456\",\"marca\":\"Nissan\"}";
    	
    	Date r2 = representacion.stringObjeto(un_string);
    	System.out.println(r2.cedula + " " + r2.nombre + " " + r2.apellido + " " + r2.chapa + " " + r2.marca);
    }
    
	
	public static String objetoString(Date d) {	
    	
		JSONObject obj = new JSONObject();
        obj.put("cedula", d.getCedula());
        obj.put("nombre", d.getNombre());
        obj.put("apellido", d.getApellido());
        obj.put("chapa", d.getChapa());
        obj.put("marca", d.getMarca());
        
        return obj.toJSONString();
    }
    
    
    public static Date stringObjeto(String str) throws Exception {
    	Date d = new Date();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

        d.setCedula((Long) jsonObject.get("cedula"));
        d.setNombre((String)jsonObject.get("nombre"));
        d.setApellido((String)jsonObject.get("apellido"));
        d.setChapa((String)jsonObject.get("chapa"));
        d.setMarca((String)jsonObject.get("marca"));
        
        return d;
	}
}
