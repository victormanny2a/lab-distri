package ejercicio82;

import java.io.*;
import java.net.*;
import java.util.*;

public class TCPClient {

    public static void main(String[] args) throws IOException {
    	int flags = 123456789;
        int opcion = 1;
        Scanner leer = new Scanner(System.in);
        
        
    	
        Socket unSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            unSocket = new Socket("localhost", 4444);
            // enviamos nosotros
            out = new PrintWriter(unSocket.getOutputStream(), true);

            //viene del servidor
            in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Host desconocido");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error de I/O en la conexion al host");
            System.exit(1);
        }

        
        String fromUser;
        String fromServer;
        while (flags != 0 && (fromServer = in.readLine()) != null) {
        	System.out.println("Servidor: " + fromServer);
        	if (flags != 123456789) {
        		for (int i=0; i<5; i++) {
        			System.out.println();
        		}
        	}
        	
        	System.out.print(" 0 - Si desea salir: ");
        	flags = leer.nextInt();
             
        	if (flags != 0) {
        		do {
            		System.out.println(" 1 - Recibir datos de personas y vehiculos");
                	System.out.println(" 2 - Consultar lista de vehiculos por CI");
                	System.out.print(" Ingrese 1 o 2: ");
                	opcion = leer.nextInt();
                	System.out.println();
                	if (opcion == 1) {
                		//System.out.println("1");
                		Date d = new Date();
                		System.out.print(" Cedula: ");
                		d.setCedula(leer.nextLong());
                		System.out.println();
                		System.out.print(" Nombre: ");
                		d.setNombre(leer.next());
                		System.out.println();
                		System.out.print(" Apellido: ");
                		d.setApellido(leer.next());
                		System.out.println();
                		System.out.print(" Chapa: ");
                		d.setChapa(leer.next());
                		System.out.println();
                		System.out.print(" Marca: ");
                		d.setMarca(leer.next());
                		System.out.println();
                		fromUser = DateJSON.objetoString(d);
                		
                		System.out.println("Cliente: " + fromUser);

                        //escribimos al servidor
                        out.println(fromUser);
                		
                	} else if (opcion == 2) {
                		//System.out.println("2");
                		System.out.print(" Ingrese cedula a consultar: ");
                		fromUser = String.valueOf(leer.nextLong());
                		System.out.println("Cliente: " + fromUser);

                        //escribimos al servidor
                        out.println(fromUser);
                	} 
            	} while (!(opcion == 1 || opcion == 2));
        	}
        	
        }
 
        out.close();
        in.close();
        unSocket.close();
    }
}
