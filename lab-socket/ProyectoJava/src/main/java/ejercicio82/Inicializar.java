package ejercicio82;

import java.util.ArrayList;
import java.util.List;

public class Inicializar {
		
	public static List<Date> initDate () {
		List<Date> listDate = new ArrayList<>();
		
		Date d1 = new Date(123456L, "Maria", "Gomez", "123ABC", "Toyota");
		Date d2 = new Date(123456L, "Maria", "Gomez", "456DEF", "Nissan");
		Date d3 = new Date(123456L, "Maria", "Gomez", "789GHI", "Volvo");
		
		Date d4 = new Date(123123L, "Ana", "Salinas", "456JKL", "Nissan");
		Date d5 = new Date(123123L, "Ana", "Salinas", "123ASD", "BMW");
		Date d6 = new Date(123123L, "Ana", "Salinas", "789IOP", "Mercedes Benz");
		
		listDate.add(d1);
		listDate.add(d2);
		listDate.add(d3);
		listDate.add(d4);
		listDate.add(d5);
		listDate.add(d6);
		
		return listDate;
	}
}
