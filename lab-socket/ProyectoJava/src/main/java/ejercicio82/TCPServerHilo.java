package ejercicio82;

import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.io.*;

public class TCPServerHilo extends Thread {

    private Socket socket = null;

    TCPMultiServer servidor;
    
    public TCPServerHilo(Socket socket, TCPMultiServer servidor ) {
        super("TCPServerHilo");
        this.socket = socket;
        this.servidor = servidor;
    }

    public void run() {
    	
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    socket.getInputStream()));
            out.println("Bienvenido!");
            String inputLine, outputLine = null;
            
            String usuario = "Users" + String.valueOf(++servidor.users);
            servidor.usuarios.add(usuario);
        	outputLine = "Usuario/a "  + usuario + " agregado";
            
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Mensaje recibido: " + inputLine);
                
                //out.println(inputLine);
                
                long valor;
                try {
                    valor = Long.parseLong(inputLine);
                    outputLine = "";
                    //System.out.println("1");
                    for (Date aux : servidor.listDate) {
                    	//System.out.println("aux: " + aux.getCedula());
                    	if (valor == aux.getCedula()) {
                    		outputLine += aux.getChapa() + " - " + aux.getMarca() + " | ";
                    		//System.out.println("out: " + outputLine);
                    	}
                    }
                    if (outputLine.equals(""))
                    	outputLine = "No se encontraron coincidencias";
                    	
                } catch (NumberFormatException | NullPointerException nfe) {
                	//System.out.println("2");
                	try {
						Date d = DateJSON.stringObjeto(inputLine);
						servidor.listDate.add(d);
						outputLine = "Se agrego correctamente";
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("Exception de JSON");
					}
                }              
                
                out.println(outputLine);
            }
            out.close();
            in.close();
            socket.close();
            System.out.println("Finalizando Hilo");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
